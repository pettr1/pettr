package com.meow.pettr.home.domain.usecases

import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.home.domain.model.HomeParameters
import javax.inject.Inject

class GetSearchFilters @Inject constructor(
  private val animalRepository: AnimalRepository
) {

  companion object {
    private const val DEFAULT_VALUE = "Any"
    private const val DEFAULT_VALUE_LOWERCASE = "any"
  }

  suspend operator fun invoke(): HomeParameters {
   // val types = animalRepository.getAnimalTypes()
//
   // val filteringTypes = if (types.any { it.toLowerCase(Locale.ROOT) == DEFAULT_VALUE_LOWERCASE }) {
   //   types
   // } else {
   //   listOf(DEFAULT_VALUE) + types
   // }
//
   // if (types.isEmpty()) throw MenuValueException("No animal types")
//
   // val ages = animalRepository.getAnimalAges()
   //     .map { it.name }
   //     .replace(AnimalWithDetails.Details.Age.UNKNOWN.name, DEFAULT_VALUE)
   //     .map { it.toLowerCase(Locale.ROOT).capitalize() }

    //Pull HomeParams from Cache.
    //Test by making new HomeParams
    return HomeParameters()
  }

  private fun List<String>.replace(old: String, new: String): List<String> {
    return map { if (it == old) new else it }
  }
}