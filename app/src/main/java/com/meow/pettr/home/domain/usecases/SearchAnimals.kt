package com.meow.pettr.home.domain.usecases

import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.home.domain.model.HomeParameters
import com.meow.pettr.home.domain.model.HomeResults
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchAnimals @Inject constructor(
    private val animalRepository: AnimalRepository
) {

  operator fun invoke(
      querySubject: BehaviorSubject<String>,
      ageSubject: BehaviorSubject<String>,
      paramSubject: BehaviorSubject<HomeParameters>
  ): Flowable<HomeResults> {
    val query = querySubject
        .debounce(500L, TimeUnit.MILLISECONDS)
        .distinctUntilChanged()

    return Observable.combineLatest(query, ageSubject, paramSubject, combiningFunction)
        .toFlowable(BackpressureStrategy.BUFFER)
        .switchMap {
            animalRepository.searchCachedAnimalsBy(it.third)
        }
  }

  private val combiningFunction: Function3<String, String, HomeParameters, Triple<String, String, HomeParameters>>
    get() = Function3 {query, age, params -> Triple(query, age, params) }
}
