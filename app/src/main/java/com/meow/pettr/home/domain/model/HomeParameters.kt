package com.meow.pettr.home.domain.model

import java.util.*

val empty = ""

data class HomeParameters(
    val name: String? = null,
    val age: String? = null,
    val type: String? = null,
    val breed: String? = null,
    val size: String? = null,
    val gender: String? = null,
    val color: String? = null,
    val coat: String? = null,
    val prefsChanged: Boolean? = false
) {
  val uppercaseName get() = name?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseAge get() = age?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseType get() = type?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseBreed get() = breed?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseSize get() = size?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseGender get() = gender?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseColor get() = color?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
  val uppercaseCoat get() = coat?.toUpperCase(Locale.ROOT)?.ifEmpty { null }
}