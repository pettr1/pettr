package com.meow.pettr.home.domain.usecases

import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.model.pagination.Pagination
import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.home.domain.MenuValueException
import com.meow.pettr.home.domain.model.HomeFilters
import com.meow.pettr.home.domain.model.HomeParameters
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.isActive
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class ClearAnimalCache @Inject constructor(
    private val animalRepository: AnimalRepository
) {
    suspend operator fun invoke(){
        animalRepository.clearUnseenCachedAnimals()
    }
}