package com.meow.pettr.home.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.snackbar.Snackbar
import com.meow.pettr.AppController.Companion.userPreferences
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.ui.AnimalsAdapter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.PetProfileFragment
import com.meow.pettr.common.ui.model.UIAnimal
import com.meow.pettr.common.utils.FirebaseUtils.firebaseAuth
import com.meow.pettr.databinding.HomeFragmentBinding
import com.meow.pettr.home.domain.model.HomeParameters
import com.meow.pettr.login.LoginActivity
import com.yuyakaido.android.cardstackview.*
import dagger.hilt.android.AndroidEntryPoint
import okio.IOException
import retrofit2.HttpException

@AndroidEntryPoint
class HomeFragment : Fragment(), CardStackListener {

    private val binding get() = _binding!!
    private var _binding: HomeFragmentBinding? = null
    private lateinit var adapter: AnimalsAdapter
    private val manager by lazy { CardStackLayoutManager(requireContext(), this) }
    private lateinit var currentAnimal: UIAnimal

    companion object {
        private const val ITEMS_PER_ROW = 2
    }

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentBinding.inflate(inflater, container, false);

        _binding!!.btnLike.setOnClickListener {
            //
            val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Right)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(AccelerateInterpolator())
                .build()
            manager.setSwipeAnimationSetting(setting)
            binding.homeRecyclerView.swipe()
        }
        _binding!!.btnPass.setOnClickListener {
            val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(AccelerateInterpolator())
                .build()
            manager.setSwipeAnimationSetting(setting)
            binding.homeRecyclerView.swipe()
        }

        _binding!!.btnProfile.setOnClickListener{
            openPetProfile()
        }

        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        prepareForSearch()

        viewModel.handleEvents(HomeEvent.ParamsValueSelected(HomeParameters(
            userPreferences?.animalName,
            userPreferences?.animalAge,
            userPreferences?.animalType,
            userPreferences?.animalBreed,
            userPreferences?.animalSize,
            userPreferences?.animalGender,
            userPreferences?.animalColor,
            userPreferences?.animalCoat,
            userPreferences?.prefsChanged)))

        userPreferences?.prefsChanged = false
    }

    private fun setupUI() {
        adapter = createAdapter()
        setupCardView(adapter)
        observeViewStateUpdates(adapter)
    }

    private fun createAdapter(): AnimalsAdapter {
        return AnimalsAdapter()
    }

    override fun onResume() {
        super.onResume()
        if(firebaseAuth.currentUser == null){
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    private fun setupCardView(searchAdapter: AnimalsAdapter) {
        adapter.notifyDataSetChanged()
        binding.homeRecyclerView.apply {
            layoutManager = manager
            manager.setStackFrom(StackFrom.None)
            manager.setVisibleCount(3)
            manager.setTranslationInterval(8.0f)
            manager.setSwipeThreshold(0.3f)
            manager.setMaxDegree(20.0f)
            manager.setDirections(Direction.HORIZONTAL)
            manager.setCanScrollHorizontal(true)
            manager.setCanScrollVertical(true)
            manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
            manager.setOverlayInterpolator(LinearInterpolator())
            adapter = searchAdapter
            itemAnimator.apply {
                if (this is DefaultItemAnimator) {
                    supportsChangeAnimations = false
                }
            }
        }
    }

    private fun observeViewStateUpdates(searchAdapter: AnimalsAdapter) {
        viewModel.state.observe(viewLifecycleOwner) {
            updateScreenState(it, searchAdapter)
        }
    }

    private fun updateScreenState(newState: SearchViewState, searchAdapter: AnimalsAdapter) {
        val (
            inInitialState,
            searchResults,
            params,
            searchingRemotely,
            noResultsState,
            failure
        ) = newState

        if(searchResults.isNotEmpty()) currentAnimal = searchResults[0]

        updateInitialStateViews(inInitialState)
        searchAdapter.submitList(searchResults)
        updateRemoteSearchViews(searchingRemotely)
        if(searchResults.size <= 1){ updateNoResultsViews(noResultsState) }
        handleFailures(failure)
    }

    private fun updateInitialStateViews(inInitialState: Boolean) {
       // binding.initialSearchImageView.isVisible = inInitialState
       // binding.initialSearchText.isVisible = inInitialState
    }

    private fun prepareForSearch() {
        viewModel.handleEvents(HomeEvent.PrepareForSearch)
    }


    private fun updateRemoteSearchViews(searchingRemotely: Boolean) {
        binding.loadingProgressBar.isVisible = searchingRemotely
        //binding.searchRemotelyText.isVisible = searchingRemotely
    }

    private fun updateNoResultsViews(noResultsState: Boolean) {
        binding.noResultsImageView.isVisible = noResultsState
        binding.tvNoMoreAnimals.isVisible = noResultsState
    }

    private fun handleFailures(failure: Event<Throwable>?) {
        val unhandledFailure = failure?.getContentIfNotHandled() ?: return
        handleThrowable(unhandledFailure)
    }

    private fun handleThrowable(exception: Throwable) {
        val fallbackMessage = "An error occurred. Please try again later."
        val snackbarMessage = when (exception) {
            is NoMoreAnimalsException -> exception.message ?: fallbackMessage
            is IOException, is HttpException -> fallbackMessage
            else -> ""
        }

        if (snackbarMessage.isNotEmpty()) {
            Snackbar.make(requireView(), snackbarMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun onUserFavorite(){
        viewModel.handleEvents(HomeEvent.UpdateAnimal("favorite"))
        if (userPreferences!!.openPetProfileOnFav){
               openPetProfile()
        }
    }

    private fun openPetProfile() {
       try {
           val petProfile = PetProfileFragment(currentAnimal)
           parentFragmentManager.beginTransaction().apply {
               setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
               add(com.meow.pettr.R.id.container, petProfile)
               setReorderingAllowed(true)
               addToBackStack(null)
               commit()
           }
       } catch (e: Exception){ }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {

    }

    override fun onCardSwiped(direction: Direction?) {
        when (direction){
            Direction.Left -> viewModel.handleEvents(HomeEvent.UpdateAnimal("passed"))
            Direction.Right -> onUserFavorite()
        }
    }

    override fun onCardRewound() {

    }

    override fun onCardCanceled() {

    }

    override fun onCardAppeared(view: View?, position: Int) {

    }

    override fun onCardDisappeared(view: View?, position: Int) {

    }

}