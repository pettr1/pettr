package com.meow.pettr.home.domain

class MenuValueException(message: String): Exception(message)