package com.meow.pettr.home.domain.model

import com.meow.pettr.common.domain.model.animal.AnimalWithDetails

data class HomeResults(
    val animals: List<AnimalWithDetails>,
    val homeParameters: HomeParameters
)