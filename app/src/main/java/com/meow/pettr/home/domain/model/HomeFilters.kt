package com.meow.pettr.home.domain.model

data class HomeFilters(
    val ages: List<String>,
    val types: List<String>
)