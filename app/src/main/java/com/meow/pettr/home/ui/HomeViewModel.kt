package com.meow.pettr.home.ui

import android.util.Log
import androidx.lifecycle.*
import com.meow.pettr.common.domain.model.LoadMoreAnimalsException
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.model.pagination.Pagination
import com.meow.pettr.common.domain.model.type.AvailableSearchFilters
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.model.mappers.UiAnimalMapper
import com.meow.pettr.common.utils.DispatchersProvider
import com.meow.pettr.common.utils.createExceptionHandler
import com.meow.pettr.home.domain.model.HomeParameters
import com.meow.pettr.home.domain.model.HomeResults
import com.meow.pettr.home.domain.usecases.*
import com.meow.pettr.user.usecases.GetTypesRemotely
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


// @BUG When the full first page returned by API has been stored
//      in cache and user set history flag, searchAnimals observer
//      will not trigger. need to handle auto load next page if
//      all animals are stored in cache.

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val searchAnimals: SearchAnimals,
    private val updateAnimal: UpdateAnimal,
    private val searchAnimalsRemotely: SearchAnimalsRemotely,
    private val clearAnimals: ClearAnimalCache,
    private val uiAnimalMapper: UiAnimalMapper,
    private val dispatchersProvider: DispatchersProvider,
    private val compositeDisposable: CompositeDisposable
): ViewModel() {

    val state: LiveData<SearchViewState>
        get() = _state

    private val PAGE_REFRESH_AT = 5
    private val _state: MutableLiveData<SearchViewState> = MutableLiveData()
    private val querySubject = BehaviorSubject.create<String>()
    private val ageSubject = BehaviorSubject.createDefault<String>("")
    private val paramSubject = BehaviorSubject.createDefault<HomeParameters>(HomeParameters())
    private var runningJobs = mutableListOf<Job>()
    private var isLastPage = false
    private var currentPage = 0

    init {
        _state.value = SearchViewState()
    }

    fun handleEvents(event: HomeEvent) {
        when(event) {
            is HomeEvent.PrepareForSearch -> prepareForSearch()
            is HomeEvent.QueryInput -> updateQuery()
            is HomeEvent.AgeValueSelected -> updateAgeValue(event.age)
            is HomeEvent.ParamsValueSelected -> updateParamsValue(event.params)
            is HomeEvent.UpdateAnimal -> updateAnimals(event.history)
        }
    }

    private fun prepareForSearch() {
        setupSearchSubscription()
    }

    private fun createExceptionHandler(message: String): CoroutineExceptionHandler {
        return viewModelScope.createExceptionHandler(message) {
            onFailure(it)
        }
    }

    private fun updateStateWithSearchValues(params: HomeParameters) {
        _state.value = state.value!!.copy(
            paramValues = params
        )
    }

    private fun setupSearchSubscription() {
        searchAnimals(querySubject, ageSubject, paramSubject)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onSearchResults(it) },
                { onFailure(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun updateQuery() {
        querySubject.onNext("search")
        setSearchingState()
    }

    private fun updateAgeValue(age: String) {
        ageSubject.onNext(age)
    }

    private fun updateParamsValue(params: HomeParameters) {
        updateStateWithSearchValues(params)

        if(params.prefsChanged!!){
            resetPagination()
            clearUnseenCachedAnimals()
        }
        paramSubject.onNext(params)
        updateQuery()
    }

    private fun resetPagination() {
        currentPage = 0
        isLastPage = false
    }

    private fun setSearchingState() {
        _state.value = state.value!!.copy(noResultsState = false, noSearchQueryState = false)
    }

    private fun updateAnimals(history: String) {
        val animalListSize = _state.value!!.searchResults.count()
        val exceptionHandler = createExceptionHandler(message = "Failed Update Animal.")

        val job = viewModelScope.launch(exceptionHandler) {
             val scope = withContext(dispatchersProvider.io()) {
                Log.d("PETTR","Updating animal...")
                 updateAnimal(history, _state.value!!.searchResults[0].id )
            }
            onUserEvent(animalListSize)
        }
        runningJobs.add(job)
        job.invokeOnCompletion {
            it?.printStackTrace()
            runningJobs.remove(job)
        }
    }

    private fun onUserEvent(listSize: Int){
        Log.d("PETTR","Animal added to favorite or passed list")
        //update animal list when cache is low and animals are available.
        //if(_state.value!!.failure != null){
        when (listSize){
            in 1..PAGE_REFRESH_AT ->  {
                searchRemotely(paramSubject.value!!)
                _state.value = state.value!!.copy(searchingRemotely = true)
            }
            0 -> {
                _state.value = state.value!!.copy(noResultsState = true)
            }
        }
    }

    private fun clearUnseenCachedAnimals(){
        val exceptionHandler = createExceptionHandler(message = "Failed Clear Animal Cache.")

        val job = viewModelScope.launch(exceptionHandler) {
            val scope = withContext(dispatchersProvider.io()) {
                Log.d("PETTR","Clearing Animal Cache...")
                clearAnimals()
            }
        }
        runningJobs.add(job)
        job.invokeOnCompletion {
            it?.printStackTrace()
            runningJobs.remove(job)
        }
    }

    private fun onSearchResults(searchResults: HomeResults) {
        val (animals, searchParameters) = searchResults

        if (animals.isEmpty()) {
            onEmptyCacheResults(searchParameters)
        } else {
            onAnimalList(animals)
        }
    }

    private fun onAnimalList(animals: List<AnimalWithDetails>) {
        _state.value = state.value!!.copy(
            noSearchQueryState = false,
            searchResults = animals.map { uiAnimalMapper.mapToView(it) },
            searchingRemotely = false,
            noResultsState = false
        )
    }

    private fun onEmptyCacheResults(searchParameters: HomeParameters) {
        searchRemotely(searchParameters)
        _state.value = state.value!!.copy(
            searchingRemotely = true
        )
    }

    private fun searchRemotely(searchParameters: HomeParameters) {
        val exceptionHandler = createExceptionHandler(message = "Failed to search remotely.")

        val job = viewModelScope.launch(exceptionHandler) {
            val pagination = withContext(dispatchersProvider.io()) {
                Log.d("PETTR","Searching remotely...")
                searchAnimalsRemotely(++currentPage, searchParameters)
            }

            onPaginationInfoObtained(pagination)
        }

        runningJobs.add(job)

        job.invokeOnCompletion {
            it?.printStackTrace()
            runningJobs.remove(job)
        }
    }

    private fun onPaginationInfoObtained(pagination: Pagination) {
        //TODO Store Pagnation info maybe.
        currentPage = pagination.currentPage
        isLastPage = !pagination.canLoadMore
    }

    private fun onFailure(throwable: Throwable) {
        when (throwable){
            is LoadMoreAnimalsException ->
                onEmptyCacheResults(paramSubject.value!!)
            is NoMoreAnimalsException ->
                _state.value = _state.value!!.copy(
                   searchingRemotely = false, noResultsState = true
               )
            else -> {
                _state.value = state.value!!.copy(failure = Event(throwable))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}