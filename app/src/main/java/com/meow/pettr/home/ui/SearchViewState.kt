package com.meow.pettr.home.ui

import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.model.UIAnimal
import com.meow.pettr.home.domain.model.HomeParameters

data class SearchViewState(
    val noSearchQueryState: Boolean = true,
    val searchResults: List<UIAnimal> = emptyList(),
    val paramValues: HomeParameters = HomeParameters(),
    val searchingRemotely: Boolean = false,
    val noResultsState: Boolean = false,
    val failure: Event<Throwable>? = null
)
