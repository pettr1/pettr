package com.meow.pettr.home.domain.usecases

import com.meow.pettr.common.domain.model.LoadMoreAnimalsException
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.pagination.Pagination
import com.meow.pettr.common.domain.model.pagination.Pagination.Companion.DEFAULT_PAGE_SIZE
import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.home.domain.model.HomeParameters
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.isActive
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class SearchAnimalsRemotely @Inject constructor(
    private val animalRepository: AnimalRepository
) {

  suspend operator fun invoke(
      pageToLoad: Int,
      homeParameters: HomeParameters,
      pageSize: Int = DEFAULT_PAGE_SIZE
  ): Pagination {
    val (animals, pagination) =
        animalRepository.searchAnimalsRemotely(pageToLoad, homeParameters, pageSize)

    if (!coroutineContext.isActive) {
      throw CancellationException("Cancelled because new data was requested")
    }

    if (animals.isEmpty()) {
      throw NoMoreAnimalsException("Couldn't find more animals that match the search parameters.")
    }

    if (animalRepository.storeAnimals(animals) <= 5){
      throw LoadMoreAnimalsException("Need to Load More Animals.")
    }

    return pagination
  }
}