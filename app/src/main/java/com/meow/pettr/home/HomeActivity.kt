package com.meow.pettr.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.meow.pettr.R
import com.meow.pettr.common.data.api.SN
import com.meow.pettr.common.utils.FileConstants
import com.meow.pettr.common.utils.FirebaseUtils
import com.meow.pettr.common.utils.PreferencesHelper
import com.meow.pettr.home.ui.HomeFragment
import com.meow.pettr.login.LoginActivity
import com.meow.pettr.user.ui.HistoryFragment
import com.meow.pettr.user.ui.FavoritesFragment
import com.meow.pettr.user.ui.ProfileFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.home_activity.*
import java.io.File

val TAG = "Pettr"
private var localFile: File? = null
val user: FirebaseUser? = FirebaseUtils.firebaseAuth.currentUser
var mFirestore = FirebaseFirestore.getInstance()

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        checkUserLogin()

    }

    override fun onResume() {
        super.onResume()
        setupNavBar()
        setupLocalStorage()
        setupAuthToken()
        checkUserFireStore()
    }

    fun checkUserLogin(){
        val user: FirebaseUser? = FirebaseUtils.firebaseAuth.currentUser
        if(user == null){
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    fun setupAuthToken(){
        PreferencesHelper.saveLastLoggedInTime(this)
        val a = SN() //API SerialNumbers
        a.sn() //API.setupNumbers
    }

    fun setupNavBar(){
        //set navbar actions
        bottomNav.setOnItemSelectedListener { item ->
            // Navigating away from Profile Fragment?
            // lets save the settings and refresh network data.
            if(supportFragmentManager.fragments.last().tag == "Profile"){
                Log.d("TEST", "YES!")
            }

            var profileFragment: Fragment = ProfileFragment()
            val homeFragment: Fragment = HomeFragment()
            val favoritesFragment: Fragment = FavoritesFragment()
            val historyFragment: Fragment = HistoryFragment()

            when (item.itemId) {
                R.id.action_home -> setCurrentFragment(homeFragment)
                R.id.action_Profile -> setCurrentFragment(profileFragment)
                R.id.action_favorites -> setCurrentFragment(favoritesFragment)
                R.id.action_history -> setCurrentFragment(historyFragment)
            }
            true
        }
    }

    private fun setCurrentFragment(fragment:Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container,fragment)
            commit()
        }

    private fun setupLocalStorage() {
        localFile = File(filesDir.absolutePath + File.separator +
                FileConstants.DATA_SOURCE_FILE_NAME)
    }

    private fun checkUserFireStore() {
        if (user != null) {
            mFirestore
                .collection("users")
                .document(user.uid)
                .get()
                .addOnSuccessListener { doc->
                    if(doc.exists()) {

                    } else {
                        createUserDocument()
                    }
                }
        }
    }
    //write default user settings on first login
    private fun createUserDocument(){
           // mFirestore.firestoreSettings = FirebaseFirestoreSettings.Builder().build()
           // if (user != null) {
           //     mFirestore
           //         .collection("users")
           //         .document(user.uid)
           //         .set(UserPreferences(username = user?.displayName.toString()), SetOptions.merge())
           //         .addOnSuccessListener {
//
           //         }
           //         .addOnFailureListener {
           //                 e -> Log.e(TAG, "Error writing document", e)
           //         }
           // }
    }

}