package com.meow.pettr.home.ui

import com.meow.pettr.home.domain.model.HomeParameters

sealed class HomeEvent {
  object PrepareForSearch : HomeEvent()
  data class QueryInput(val input: String): HomeEvent()
  data class AgeValueSelected(val age: String): HomeEvent()
  data class ParamsValueSelected(val params: HomeParameters): HomeEvent()
  data class UpdateAnimal(val history: String): HomeEvent()
  object LoadAnimals: HomeEvent()
}