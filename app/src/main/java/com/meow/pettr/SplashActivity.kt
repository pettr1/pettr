package com.meow.pettr

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseUser
import com.meow.pettr.common.utils.FirebaseUtils
import com.meow.pettr.home.HomeActivity
import com.meow.pettr.login.LoginActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //determine if user is logged in and show main or login activity.
        val user: FirebaseUser? = FirebaseUtils.firebaseAuth.currentUser
        if(user == null) {
            startActivity(Intent(this, LoginActivity::class.java))
        } else {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        finish()

    }
}