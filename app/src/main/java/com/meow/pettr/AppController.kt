package com.meow.pettr

import android.app.Application
import androidx.room.Room
import com.meow.pettr.common.data.cache.ReportDatabase
import com.meow.pettr.common.domain.model.user.UserPreferences
import dagger.hilt.android.HiltAndroidApp

val userPreferences: UserPreferences by lazy {
    AppController.userPreferences!!
}

@HiltAndroidApp
class AppController : Application() {

    companion object {
        var userPreferences: UserPreferences? = null
        var database: ReportDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        initReporter()
        userPreferences = UserPreferences(applicationContext)
    }

    private fun initReporter() {
        database = Room.databaseBuilder(this, ReportDatabase::class.java,
            "reports.db").build()
    }

}