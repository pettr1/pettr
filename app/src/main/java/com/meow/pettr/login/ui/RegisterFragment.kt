package com.meow.pettr.login.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.meow.pettr.R
import com.meow.pettr.databinding.RegisterFragmentBinding
import com.meow.pettr.common.utils.FirebaseUtils.firebaseAuth
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    companion object {
        fun newInstance() = RegisterFragment()
    }

    private lateinit var viewModel: LoginViewModel
    lateinit var binding: RegisterFragmentBinding
    lateinit var userEmail: String
    lateinit var userPassword: String
    lateinit var mview: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {

        binding = RegisterFragmentBinding.inflate(inflater, container, false)

        var login = binding.textViewLogin
        login.setOnClickListener{
            Navigation.findNavController(it).popBackStack()
        }

        var register = binding.buttonRegister
        register.setOnClickListener{
            register()
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        // TODO: Use the ViewModel

    }

    fun register(){

        userEmail = binding.emailInput.text.toString()
        userPassword = binding.editTextPassword.text.toString()

        //create a user
        firebaseAuth.createUserWithEmailAndPassword(userEmail, userPassword)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    sendEmailVerification()
                    Toast.makeText(
                        context, task.result.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    Navigation.findNavController(binding.root).navigate(R.id.action_registerFragment_to_loginFragment)
                } else {
                    //toast("failed to Authenticate !")
                    Log.d("Firebase Error", task.exception.toString())
                    Toast.makeText(
                        context, task.exception.toString(),
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
    }

    //not working
    private fun sendEmailVerification() {
        firebaseAuth.currentUser?.let {
            it.sendEmailVerification().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    //toast("email sent to $userEmail")
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

}