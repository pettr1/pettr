package com.meow.pettr.login.ui

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.meow.pettr.R
import com.meow.pettr.common.utils.FirebaseUtils.firebaseAuth
import com.meow.pettr.databinding.LoginFragmentBinding
import com.meow.pettr.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }
    private lateinit var viewModel: LoginViewModel
    private lateinit var auth: FirebaseAuth
    private var RC_SIGN_IN: Int = 9001
    lateinit var binding: LoginFragmentBinding
    lateinit var userEmail: String
    lateinit var userPassword: String
    lateinit var mGoogleSignInClient: GoogleSignInClient

    private var resultLauncher =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(result.data)
            try {
                val account: GoogleSignInAccount = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Log.w("pettr", "Google sign in failed", e)
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        val clientId: String = getString(R.string.client_id)
        var mcontext: Context = requireActivity().application
        binding = LoginFragmentBinding.inflate(layoutInflater, container, false);

        firebaseAuth = FirebaseAuth.getInstance()

        val account = GoogleSignIn.getLastSignedInAccount(mcontext)

        if (account != null){
            startMain()
        }

       //Google Sign in
       val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
           .requestIdToken(clientId)
           .requestEmail()
           .build()

        mGoogleSignInClient = GoogleSignIn.getClient(mcontext, gso)


        var gSignin = binding.gsigninButton
        gSignin.setOnClickListener{
            resultLauncher.launch(mGoogleSignInClient.signInIntent)
        }

        var forgot = binding.forgotPassword
        forgot.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
        }

        var register = binding.textViewRegister
        register.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_registerFragment)
        }
        var login = binding.buttonLogin
        login.setOnClickListener{
            signInEmail()
        }
        return binding.root;
    }

    override fun onStart() {
        super.onStart()
        //Check if user is already logged in
        if(firebaseAuth.currentUser != null){
            startMain()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    binding.editTextPassword.setText("")
                    binding.emailInput.setText("")
                    startActivity(Intent(context, HomeActivity::class.java))
                } else {
                    Log.w("pettr", "signInWithGoogle:failure", task.exception)
                    Toast.makeText(
                        context, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            .addOnFailureListener { e ->
                Log.w("pettr", "signInWithEmail:failure", e)
                Toast.makeText(
                    context, "Authentication failed.",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    private fun signInEmail() {
        userEmail = binding.emailInput.text.toString().trim()
        userPassword = binding.editTextPassword.text.toString().trim()

        if(userEmail.isNotEmpty() && userPassword.isNotEmpty()) {
            firebaseAuth.signInWithEmailAndPassword(userEmail, userPassword)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("pettr", "signInWithEmail:success")

                        //First time User?
                        if (checkNewUser()) {
                            //go to profile
                        }
                        binding.editTextPassword.setText("")
                        binding.emailInput.setText("")
                        startActivity(Intent(context, HomeActivity::class.java))
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("pettr", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            context, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                .addOnFailureListener { task ->
                    Log.w("pettr", "signInWithEmail:failure : " + task.message.toString().trim())
                }
        }
    }

    private fun startMain(){
        startActivity(Intent(context, HomeActivity::class.java))
    }

    private fun checkNewUser(): Boolean {

        return true
    }

}//end loginFragment