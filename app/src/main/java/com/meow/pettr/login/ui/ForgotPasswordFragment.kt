package com.meow.pettr.login.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import com.google.firebase.auth.FirebaseAuth
import com.meow.pettr.R
import com.meow.pettr.databinding.ForgotPasswordFragmentBinding
import com.meow.pettr.databinding.LoginFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : Fragment() {

    companion object {
        fun newInstance() = ForgotPasswordFragment()
    }

    private lateinit var viewModel: ForgotPasswordViewModel
    lateinit var binding: ForgotPasswordFragmentBinding
    private var mAuth: FirebaseAuth? = null
    private var email: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = ForgotPasswordFragmentBinding.inflate(layoutInflater, container, false);
        mAuth = FirebaseAuth.getInstance()

        var resetPassword = binding.buttonForgot.setOnClickListener{
            email = binding.forgotEmail.text.toString()
            if (!email.isNullOrBlank()) {
                mAuth!!.sendPasswordResetEmail(email!!)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                requireContext(),
                                "Reset password email has been sent.",
                                LENGTH_LONG
                            )
                                .show()
                        } else {
                            Toast.makeText(
                                requireContext(),
                                task.exception?.message ?: "Failed",
                                LENGTH_LONG
                            )
                                .show()
                        }
                    }
            }
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        // TODO: Use the ViewModel
    }

}