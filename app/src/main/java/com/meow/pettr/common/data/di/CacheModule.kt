package com.meow.pettr.common.data.di

import android.content.Context
import androidx.room.Room
import com.meow.pettr.common.data.cache.Cache
import com.meow.pettr.common.data.cache.LocalDatabase
import com.meow.pettr.common.data.cache.RoomCache
import com.meow.pettr.common.data.cache.model.daos.AnimalsDao
import com.meow.pettr.common.data.cache.model.daos.OrganizationsDao
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheModule {

  @Binds
  abstract fun bindCache(cache: RoomCache): Cache

  companion object {

    @Provides
    fun provideDatabase(@ApplicationContext context: Context): LocalDatabase {
      return Room.databaseBuilder(context, LocalDatabase::class.java, "pettr.db")
          .build()
    }

    @Provides
    fun provideAnimalsDao(petSaveDatabase: LocalDatabase): AnimalsDao =
        petSaveDatabase.animalsDao()

    @Provides
    fun provideOrganizationsDao(petSaveDatabase: LocalDatabase): OrganizationsDao =
        petSaveDatabase.organizationsDao()
  }
}