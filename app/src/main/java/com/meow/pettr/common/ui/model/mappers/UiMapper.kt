package com.meow.pettr.common.ui.model.mappers

interface UiMapper<E, V> {

  fun mapToView(input: E): V
}