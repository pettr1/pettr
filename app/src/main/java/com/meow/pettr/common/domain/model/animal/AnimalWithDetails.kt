package com.meow.pettr.common.domain.model.animal

import com.meow.pettr.common.domain.model.organization.Organization
import java.time.LocalDateTime

data class AnimalWithDetails(
    val id: Long,
    val name: String,
    val type: String,
    val history: String,
    val details: Details,
    val media: Media,
    val tags: List<String>,
    val adoptionStatus: AdoptionStatus,
    val publishedAt: LocalDateTime,
    val timeAdded: LocalDateTime
) {

  fun withNoDetails(): Animal {
    return Animal(id, name, type, media, tags, adoptionStatus, publishedAt)
  }

  data class Details(
      val description: String,
      val age: Age,
      val species: String,
      val breed: Breed,
      val colors: Colors,
      val gender: Gender,
      val size: Size,
      val coat: Coat,
      val healthDetails: HealthDetails,
      val habitatAdaptation: HabitatAdaptation,
      val organization: Organization
  ) {
    enum class Age {
      UNKNOWN,
      BABY,
      YOUNG,
      ADULT,
      SENIOR
    }

    data class Breed(
        val primary: String,
        val secondary: String
    ) {
      val mixed: Boolean
        get() = primary.isNotEmpty() && secondary.isNotEmpty()

      val unknown: Boolean
        get() = primary.isEmpty() && secondary.isEmpty()
    }

    data class Colors(
        val primary: String,
        val secondary: String,
        val tertiary: String
    )

    enum class Gender{
      UNKNOWN,
      FEMALE,
      MALE
    }

    enum class Size (val sizestring: String) {
      UNKNOWN("Unknown"),
      SMALL("Small"),
      MEDIUM("Medium"),
      LARGE("Large"),
      EXTRA_LARGE("Extra Large")
    }

    enum class Coat (val coatstring: String){
      UNKNOWN("Unknown"),
      SHORT("Short"),
      MEDIUM("Medium"),
      LONG("Long"),
      WIRE("Wire"),
      HAIRLESS("Hairless"),
      CURLY("Curly")
    }

    data class HealthDetails(
        val isSpayedOrNeutered: Boolean,
        val isDeclawed: Boolean,
        val hasSpecialNeeds: Boolean,
        val shotsAreCurrent: Boolean
    )

    data class HabitatAdaptation(
        val goodWithChildren: Boolean,
        val goodWithDogs: Boolean,
        val goodWithCats: Boolean
    )
  }
}