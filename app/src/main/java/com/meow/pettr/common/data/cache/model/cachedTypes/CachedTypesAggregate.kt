package com.meow.pettr.common.data.cache.model.cachedTypes

import androidx.room.*
import com.meow.pettr.common.data.cache.model.cachedanimal.*
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails

data class CachedTypesAggregate(
    @Embedded
    val animal: CachedAnimalWithDetails,
    @Relation(
        parentColumn = "typeId",
        entityColumn = "typeId"
    )
    val photos: List<CachedPhoto>,
    @Relation(
        parentColumn = "typeId",
        entityColumn = "typeId"
    )
    val videos: List<CachedVideo>,
    @Relation(
        parentColumn = "typeId",
        entityColumn = "typeId",
        associateBy = Junction(CachedAnimalTagCrossRef::class)
    )
    val tags: List<CachedTag>
) {

    companion object {
        fun fromDomain(animalWithDetails: AnimalWithDetails): CachedAnimalAggregate {
            return CachedAnimalAggregate(
                animal = CachedAnimalWithDetails.fromDomain(animalWithDetails),
                photos = animalWithDetails.media.photos.map {
                    CachedPhoto.fromDomain(animalWithDetails.id, it)
                },
                videos = animalWithDetails.media.videos.map {
                    CachedVideo.fromDomain(animalWithDetails.id, it)
                },
                tags =  animalWithDetails.tags.map { CachedTag(it) }
            )
        }
    }
}