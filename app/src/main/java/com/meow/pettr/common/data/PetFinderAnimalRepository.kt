package com.meow.pettr.common.data

import com.meow.pettr.common.data.api.PetFinderApi
import com.meow.pettr.common.data.api.model.mappers.ApiAnimalMapper
import com.meow.pettr.common.data.api.model.mappers.ApiPaginationMapper
import com.meow.pettr.common.data.api.model.mappers.ApiTypeMapper
import com.meow.pettr.common.data.cache.Cache
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization
import com.meow.pettr.common.domain.model.pagination.PaginatedAnimals
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.model.type.AvailableSearchFilters
import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.common.utils.DispatchersProvider
import com.meow.pettr.home.domain.model.HomeParameters
import com.meow.pettr.home.domain.model.HomeResults
import com.meow.pettr.userPreferences
import dagger.hilt.android.scopes.ActivityRetainedScoped
import io.reactivex.Flowable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import javax.inject.Inject

@ActivityRetainedScoped
class PetFinderAnimalRepository @Inject constructor(
    private val api: PetFinderApi,
    private val cache: Cache,
    private val apiAnimalMapper: ApiAnimalMapper,
    private val apiTypeMapper: ApiTypeMapper,
    private val apiPaginationMapper: ApiPaginationMapper,
    dispatchersProvider: DispatchersProvider
): AnimalRepository {

  private val parentJob = SupervisorJob()
  private val repositoryScope = CoroutineScope(parentJob + dispatchersProvider.io())

  // fetch these from shared preferences, after storing them in onboarding screen
  private val postcode = "48030"
  private val maxDistanceMiles = 500

  override fun getAnimals(): Flowable<List<AnimalWithDetails>> {
    return cache.getNearbyAnimals()
        .distinctUntilChanged()
        .map { animalList ->
          animalList.map { it.animal.toDomain(it.photos, it.videos, it.tags, cache.getOrganization(it.animal.organizationId)) }
        }
  }

  override fun getHistoricAnimals(): Flowable<List<AnimalWithDetails>> {
      return cache.getHistoricAnimals()
          .distinctUntilChanged()
          .map { animalList ->
              animalList.map { it.animal.toDomain(it.photos, it.videos, it.tags, cache.getOrganization(it.animal.organizationId)) }
          }
  }

  override fun getFavoriteAnimals(): Flowable<List<AnimalWithDetails>> {
      return cache.getFavoriteAnimals()
          .distinctUntilChanged()
          .map { animalList ->
              animalList.map { it.animal.toDomain(it.photos, it.videos, it.tags, cache.getOrganization(it.animal.organizationId)) }
          }
  }

  override suspend fun requestMoreAnimals(
      pageToLoad: Int,
      numberOfItems: Int
  ): PaginatedAnimals {
      val (apiAnimals, apiPagination) = api.getNearbyAnimals(
          pageToLoad,
          numberOfItems,
          postcode,
          maxDistanceMiles
      )

      return PaginatedAnimals(
          apiAnimals?.map { apiAnimalMapper.mapToDomain(it) }.orEmpty(),
          apiPaginationMapper.mapToDomain(apiPagination)
      )
  }

  override suspend fun storeAnimals(animals: List<AnimalWithDetails>): Int {
    var animalsFiltered = checkAnimalsInCache(animals)
    val organizations = animalsFiltered.map { CachedOrganization.fromDomain(it.details.organization) }

    cache.storeOrganizations(organizations)
    cache.storeNearbyAnimals(animalsFiltered.map { CachedAnimalAggregate.fromDomain(it) })

    return animalsFiltered.size
  }

  private fun checkAnimalsInCache(animals: List<AnimalWithDetails>)
  :List<AnimalWithDetails> {
      var filteredAnimalList = animals.filter {
          !cache.isAnimalInCache(it)
      }
      return filteredAnimalList
  }

  override suspend fun updateAnimalHistory(history: String, animalId: Long) {
      cache.updateAnimalHistory(history, animalId)
  }

  override suspend fun getAnimalTypes(): List<String> {
    return cache.getAllTypes()
  }

  override fun getAnimalAges(): List<AnimalWithDetails.Details.Age> {
    return AnimalWithDetails.Details.Age.values().toList()
  }

  override fun searchCachedAnimalsBy(homeParameters: HomeParameters): Flowable<HomeResults> {
    return cache.searchAnimalsBy()
        .map { animalList ->
          animalList.map { it.animal.toDomain(it.photos, it.videos, it.tags, cache.getOrganization(it.animal.organizationId)) }
        }
        .map { HomeResults(it, homeParameters) }
  }

  override fun removeCachedAnimal(animalId: Long) {
      cache.removeAnimal(animalId)
  }

  override fun clearUnseenCachedAnimals(){
      cache.clearCache()
  }

  override suspend fun searchAnimalsRemotely(
      pageToLoad: Int,
      homeParameters: HomeParameters,
      numberOfItems: Int
  ): PaginatedAnimals {

    val (apiAnimals, apiPagination) = api.searchAnimalsBy(
        convertToNull(homeParameters.name),
        convertToNull(homeParameters.age),
        convertToNull(homeParameters.type),
        convertToNull(homeParameters.breed),
        convertToNull(homeParameters.size),
        convertToNull(homeParameters.gender),
        convertToNull(homeParameters.color),
        convertToNull(homeParameters.coat),
        pageToLoad,
        numberOfItems,
        userPreferences.animalLocation,
        userPreferences.animalDistance
    )

    return PaginatedAnimals(
        apiAnimals?.map { apiAnimalMapper.mapToDomain(it) }.orEmpty(),
        apiPaginationMapper.mapToDomain(apiPagination)
    )
  }

    private fun convertToNull(value: String?):String?{
        return when(value) {
            "" -> null
            else -> value
        }
    }

  override suspend fun getTypesRemotely(): AvailableSearchFilters {
      val types = api.getAvailableTypes().types

      return AvailableSearchFilters(
          types?.map { apiTypeMapper.mapToDomain(it) }.orEmpty()
      )
  }

  override suspend fun clearAllCache() {
      cache.clearAllCache()
  }
}
