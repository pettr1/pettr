package com.meow.pettr.common.ui.model

data class UIAnimal(
    val id: Long,
    val name: String,
    val age: String,
    val breedPrimary: String,
    val breedSecondary: String,
    val colorPrimary: String,
    val colorSecondary: String,
    val gender: String,
    val size: String,
    val coat: String,
    val history: String,
    val photo: String,
    val description: String,
    val email: String,
    val phone: String,
    val address: String,
    val city: String,
    val postcode: String,
    val country: String,
    val distance: String

)