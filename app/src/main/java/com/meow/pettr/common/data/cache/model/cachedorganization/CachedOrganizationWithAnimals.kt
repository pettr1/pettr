package com.meow.pettr.common.data.cache.model.cachedorganization

import androidx.room.Embedded
import androidx.room.Relation
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalWithDetails
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization

data class CachedOrganizationWithAnimals(
    @Embedded val organization: CachedOrganization,
    @Relation(
        entity = CachedAnimalWithDetails::class,
        parentColumn = "id",
        entityColumn = "organizationId"
    )
    val animals: List<CachedAnimalAggregate>
)