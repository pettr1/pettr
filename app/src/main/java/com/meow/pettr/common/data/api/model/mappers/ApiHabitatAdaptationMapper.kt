package com.meow.pettr.common.data.api.model.mappers

import com.meow.pettr.common.data.api.model.ApiEnvironment
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.data.api.model.mappers.ApiMapper
import javax.inject.Inject

class ApiHabitatAdaptationMapper @Inject constructor():
    ApiMapper<ApiEnvironment?, AnimalWithDetails.Details.HabitatAdaptation> {

  override fun mapToDomain(apiEntity: ApiEnvironment?): AnimalWithDetails.Details.HabitatAdaptation {
    return AnimalWithDetails.Details.HabitatAdaptation(
        goodWithChildren = apiEntity?.children ?: true,
        goodWithDogs = apiEntity?.dogs ?: true,
        goodWithCats = apiEntity?.cats ?: true
    )
  }
}
