package com.meow.pettr.common.domain.model.pagination

import com.meow.pettr.common.domain.model.animal.AnimalWithDetails

data class PaginatedAnimals(
    val animals: List<AnimalWithDetails>,
    val pagination: Pagination
)