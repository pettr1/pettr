package com.meow.pettr.common.data.api.model.mappers

import com.meow.pettr.common.data.api.model.ApiBreeds
import com.meow.pettr.common.data.api.model.mappers.ApiMapper
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import javax.inject.Inject

class ApiBreedsMapper @Inject constructor():
    ApiMapper<ApiBreeds?, AnimalWithDetails.Details.Breed> {

  override fun mapToDomain(apiEntity: ApiBreeds?): AnimalWithDetails.Details.Breed {
    return AnimalWithDetails.Details.Breed(
        primary = apiEntity?.primary.orEmpty(),
        secondary = apiEntity?.secondary.orEmpty()
    )
  }
}