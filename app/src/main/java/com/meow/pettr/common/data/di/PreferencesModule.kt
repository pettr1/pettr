package com.meow.pettr.common.data.di

import com.meow.pettr.common.data.preferences.AppPreferences
import com.meow.pettr.common.data.preferences.Preferences
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PreferencesModule {

  @Binds
  abstract fun providePreferences(preferences: AppPreferences): Preferences
}