package com.meow.pettr.common.data.api

import androidx.annotation.Keep
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

@Keep
class SN { //SerialNumbers
  fun sn() { //setupNumbers

    //setValue
    sv("com.meow.pettr.common.data.api.GO", "f1", 7)
    sv("com.meow.pettr.common.data.api.GO", "f2", 3)
    sv("com.meow.pettr.common.data.api.GO", "f3", 9)
  }
}

@Keep
object GO {
  var f1 = 3 //field1
  var f2 = 1 //field2
  var f3 = 5 //field3
}

fun sv(ownerClassName: String, fieldName: String, value: Any) { //setValue - uses reflection
  val kClass = Class.forName(ownerClassName).kotlin
  val instance = kClass.objectInstance ?: kClass.java.newInstance()

  val member = kClass.memberProperties.filterIsInstance<KMutableProperty<*>>()
      .firstOrNull { it.name == fieldName }

  member?.setter?.call(instance, value)
}