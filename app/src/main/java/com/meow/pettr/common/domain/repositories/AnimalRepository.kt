package com.meow.pettr.common.domain.repositories

import com.meow.pettr.common.domain.model.pagination.PaginatedAnimals
import com.meow.pettr.common.domain.model.animal.Animal
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.model.type.AvailableSearchFilters
import com.meow.pettr.home.domain.model.HomeParameters
import com.meow.pettr.home.domain.model.HomeResults
import io.reactivex.Flowable

interface AnimalRepository {

  fun getAnimals(): Flowable<List<AnimalWithDetails>>
  fun getHistoricAnimals(): Flowable<List<AnimalWithDetails>>
  fun getFavoriteAnimals(): Flowable<List<AnimalWithDetails>>
  suspend fun requestMoreAnimals(pageToLoad: Int, numberOfItems: Int): PaginatedAnimals
  suspend fun storeAnimals(animals: List<AnimalWithDetails>): Int
  suspend fun getAnimalTypes(): List<String>
  fun getAnimalAges(): List<AnimalWithDetails.Details.Age>
  fun searchCachedAnimalsBy(homeParameters: HomeParameters): Flowable<HomeResults>
  suspend fun updateAnimalHistory(history: String, animalId: Long)
  fun removeCachedAnimal(animalId: Long)
  suspend fun clearAllCache()
  fun clearUnseenCachedAnimals()
  suspend fun getTypesRemotely(): AvailableSearchFilters

  suspend fun searchAnimalsRemotely(
      pageToLoad: Int,
      homeParameters: HomeParameters,
      numberOfItems: Int
  ): PaginatedAnimals
}