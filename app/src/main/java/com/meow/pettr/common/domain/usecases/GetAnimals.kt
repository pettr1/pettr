package com.meow.pettr.common.domain.usecases

import com.meow.pettr.common.domain.repositories.AnimalRepository
import javax.inject.Inject

class GetAnimals @Inject constructor(
    private val animalRepository: AnimalRepository
) {

  operator fun invoke() = animalRepository.getAnimals()
      .filter { it.isNotEmpty() }
}