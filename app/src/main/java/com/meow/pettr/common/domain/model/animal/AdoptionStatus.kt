package com.meow.pettr.common.domain.model.animal

enum class AdoptionStatus {
  UNKNOWN,
  ADOPTABLE,
  ADOPTED,
  FOUND
}