package com.meow.pettr.common.data.cache.model.daos

import androidx.room.*
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalWithDetails
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedPhoto
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedTag
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import io.reactivex.Flowable

@Dao
abstract class AnimalsDao {

  @Transaction
  @Query("SELECT EXISTS (SELECT * FROM animals WHERE animalId == :id)")
  abstract fun animalExists(id: Long): Boolean

  @Query("SELECT * FROM animals ORDER BY timeAdded DESC")
  abstract fun getAllAnimals(): Flowable<List<CachedAnimalAggregate>>

  @Query("SELECT * FROM animals WHERE history == 'favorite' OR history == 'passed' ORDER BY timeAdded DESC")
  abstract fun getHistoricAnimals(): Flowable<List<CachedAnimalAggregate>>

  @Query("SELECT * FROM animals WHERE history == 'favorite' ORDER BY timeAdded DESC")
  abstract fun getFavoriteAnimals(): Flowable<List<CachedAnimalAggregate>>

  @Query("SELECT history FROM animals WHERE animalId == :id")
  abstract fun isAnimalHandled(id: Long): String

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  abstract fun insertAnimalAggregate(
      animal: CachedAnimalWithDetails,
      photos: List<CachedPhoto>,
      tags: List<CachedTag>
  )

  fun insertAnimalsWithDetails(animalAggregates: List<CachedAnimalAggregate>) {
      for (animalAggregate in animalAggregates) {
          var animalHistory = isAnimalHandled(animalAggregate.animal.animalId)
          if(animalHistory == "" || animalHistory == null){
              insertAnimalAggregate(
                  animalAggregate.animal,
                  animalAggregate.photos,
                  animalAggregate.tags
              )
          }
      }
  }

  @Query("DELETE FROM animals WHERE animalId = :animalId")
  abstract fun delete(animalId: Long)

  @Query("DELETE FROM animals WHERE history = ''")
  abstract fun clearCache()

  @Query("DELETE FROM animals")
  abstract fun clearAllCache()

  @Query("UPDATE animals SET history=:history, timeAdded=:time WHERE animalId = :id")
  abstract fun updateAnimalHistory(history: String?, id: Long, time: String)

  @Query("SELECT DISTINCT type FROM animals")
  abstract suspend fun getAllTypes(): List<String>

  @Transaction
  @Query("""
      SELECT * FROM animals 
        WHERE history = ""
        ORDER BY timeAdded ASC
  """)
  abstract fun searchAnimalsBy(
  ): Flowable<List<CachedAnimalAggregate>>
}
