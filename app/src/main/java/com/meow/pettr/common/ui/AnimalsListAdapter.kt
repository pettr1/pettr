package com.meow.pettr.common.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.meow.pettr.R
import com.meow.pettr.common.ui.model.UIAnimal
import com.meow.pettr.common.utils.setImage
import com.meow.pettr.databinding.PetListItemBinding
import kotlinx.android.synthetic.main.login_fragment.view.*


class AnimalsListAdapter(private val onPetClick: (UIAnimal?) -> Unit): ListAdapter<UIAnimal, AnimalsListAdapter.AnimalsListViewHolder>(
    ITEM_COMPARATOR
) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalsListViewHolder {
    val binding = PetListItemBinding
        .inflate(LayoutInflater.from(parent.context), parent, false)

    return AnimalsListViewHolder(binding)
  }

  override fun onBindViewHolder(holder: AnimalsListViewHolder, position: Int) {
    val item: UIAnimal = getItem(position)

    holder.bind(item, onPetClick)
  }

  class AnimalsListViewHolder(
      private val binding: PetListItemBinding
  ) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: UIAnimal, onPetClick: (UIAnimal?) -> Unit) {
      binding.petNameData.text = item.name
      binding.petProfilePhoto.setImage(item.photo)
      if(item.history == "favorite") {
        binding.historyData.setImageResource(R.drawable.ic_loved)
      }else{
        binding.historyData.setImageResource(R.drawable.ic_not_loved)
      }
      binding.breedData.text = item.breedPrimary
      binding.root.setOnClickListener {
        onPetClick(item)
      }
    }
  }
}

private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<UIAnimal>() {
  override fun areItemsTheSame(oldItem: UIAnimal, newItem: UIAnimal): Boolean {
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: UIAnimal, newItem: UIAnimal): Boolean {
    return oldItem.name == newItem.name && oldItem.photo == newItem.photo
  }
}