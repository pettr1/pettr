package com.meow.pettr.common.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.meow.pettr.common.data.cache.model.cachedanimal.*
import com.meow.pettr.common.data.cache.model.daos.AnimalsDao
import com.meow.pettr.common.data.cache.model.daos.OrganizationsDao
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization

//import com.meow.pettr.common.data.cache.model.daos.UserDao

@Database(
    entities = [
      CachedAnimalWithDetails::class,
      CachedPhoto::class,
      CachedVideo::class,
      CachedTag::class,
      CachedAnimalTagCrossRef::class,
      CachedOrganization::class
      //CachedUserPreferences::class
    ],
    version = 1,
    exportSchema = false
)
abstract class LocalDatabase : RoomDatabase() {
  abstract fun animalsDao(): AnimalsDao
  abstract fun organizationsDao(): OrganizationsDao
  //abstract fun userPrefsDao(): UserDao
}