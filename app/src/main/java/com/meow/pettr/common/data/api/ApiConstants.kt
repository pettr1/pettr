package com.meow.pettr.common.data.api

object ApiConstants {
  const val BASE_ENDPOINT = "https://api.petfinder.com/v2/"
  const val AUTH_ENDPOINT = "oauth2/token/"
  const val ANIMALS_ENDPOINT = "animals"
  const val TYPES_ENDPOINT = "types"

  const val KEY = "dmIq9S8t6qgHYbcuAdO9r2vlWibl7AhzoDbrqNmJr9AHjqvU52"
  const val SECRET = "jH50KfveWFk7Pn5BgljTcq2iJ7UKlwQd1oqLtOTl"

}

object ApiParameters {
  const val TOKEN_TYPE = "Bearer "
  const val AUTH_HEADER = "Authorization"
  const val GRANT_TYPE_KEY = "grant_type"
  const val GRANT_TYPE_VALUE = "client_credentials"
  const val CLIENT_ID = "client_id"
  const val CLIENT_SECRET = "client_secret"

  const val PAGE = "page"
  const val LIMIT = "limit"
  const val LOCATION = "location"
  const val DISTANCE = "distance"
  const val NAME = "name"
  const val AGE = "age"
  const val TYPE = "type"
  const val TYPES = "types"
  const val BREED = "breed"
  const val SIZE = "size"
  const val GENDER = "gender"
  const val COLOR = "color"
  const val COAT = "coat"
  const val SORT = "sort"
}