package com.meow.pettr.common.data.api.model

import com.meow.pettr.common.domain.model.type.Breeds
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiTypes(
    @field:Json(name = "types") val types: List<ApiType>?
)

@JsonClass(generateAdapter = true)
data class ApiType(
    @field:Json(name = "name") val type: String?,
    @field:Json(name = "coats") val coats: List<String>?,
    @field:Json(name = "colors") val colors: List<String>?,
    @field:Json(name = "breeds") val breeds: ApiBreedList?
)

@JsonClass(generateAdapter = true)
data class ApiBreedList(
    @field:Json(name = "name") val type: String?,
    @field:Json(name = "breed") val breed: String?
)