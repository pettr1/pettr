package com.meow.pettr.common.domain.model.type

data class Breeds(
    val type: String,
    val breeds: String
)