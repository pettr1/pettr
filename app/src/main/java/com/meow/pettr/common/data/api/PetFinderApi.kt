package com.meow.pettr.common.data.api

import com.meow.pettr.common.data.api.model.ApiPaginatedAnimals
import com.meow.pettr.common.data.api.model.ApiTypes
import retrofit2.http.GET
import retrofit2.http.Query

interface PetFinderApi {

  @GET(ApiConstants.ANIMALS_ENDPOINT)
  suspend fun getNearbyAnimals(
      @Query(ApiParameters.PAGE) pageToLoad: Int?,
      @Query(ApiParameters.LIMIT) pageSize: Int?,
      @Query(ApiParameters.LOCATION) postcode: String?,
      @Query(ApiParameters.DISTANCE) maxDistance: Int?
  ): ApiPaginatedAnimals

  @GET(ApiConstants.ANIMALS_ENDPOINT)
  suspend fun searchAnimalsBy(
      @Query(ApiParameters.NAME) name: String? = null,
      @Query(ApiParameters.AGE, encoded = true) age: String? = null,
      @Query(ApiParameters.TYPE, encoded = true) type: String? = null,
      @Query(ApiParameters.BREED, encoded = true) breed: String? = null,
      @Query(ApiParameters.SIZE, encoded = true) size: String? = null,
      @Query(ApiParameters.GENDER, encoded = true) gender: String? = null,
      @Query(ApiParameters.COLOR, encoded = true) color: String? = null,
      @Query(ApiParameters.COAT, encoded = true) coat: String? = null,
      @Query(ApiParameters.PAGE) pageToLoad: Int? = null,
      @Query(ApiParameters.LIMIT) pageSize: Int? = 20,
      @Query(ApiParameters.LOCATION) postcode: String? = null,
      @Query(ApiParameters.DISTANCE) maxDistance: Int? = 100,
      @Query(ApiParameters.SORT) sortBy: String? = "distance"
  ): ApiPaginatedAnimals

  @GET(ApiConstants.ANIMALS_ENDPOINT)
  suspend fun getAvailableBreeds(
        @Query(ApiParameters.TYPE) type: String?,
        @Query(ApiParameters.TYPES) types: String?
    ): ApiTypes

  @GET(ApiConstants.TYPES_ENDPOINT)
  suspend fun getAvailableTypes(): ApiTypes
}