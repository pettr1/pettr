package com.meow.pettr.common.data.api.model.mappers

import com.meow.pettr.common.data.api.model.ApiColors
import com.meow.pettr.common.data.api.model.mappers.ApiMapper
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import javax.inject.Inject

class ApiColorsMapper @Inject constructor():
    ApiMapper<ApiColors?, AnimalWithDetails.Details.Colors> {

  override fun mapToDomain(apiEntity: ApiColors?): AnimalWithDetails.Details.Colors {
    return AnimalWithDetails.Details.Colors(
        primary = apiEntity?.primary.orEmpty(),
        secondary = apiEntity?.secondary.orEmpty(),
        tertiary = apiEntity?.tertiary.orEmpty()
    )
  }
}
