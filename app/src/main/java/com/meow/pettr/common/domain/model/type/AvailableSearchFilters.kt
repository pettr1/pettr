package com.meow.pettr.common.domain.model.type

data class AvailableSearchFilters(
    val types: List<SearchFilter>
)
data class SearchFilter(
    val type: String,
    val coats: List<String>,
    val colors: List<String>,
    val breeds: Breeds
)
