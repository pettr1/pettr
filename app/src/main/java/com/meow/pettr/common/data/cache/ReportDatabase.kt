package com.meow.pettr.common.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.meow.pettr.common.data.cache.model.daos.ReportsDao
import com.meow.pettr.common.data.cache.model.Report

@Database(entities = [(Report::class)], version = 1, exportSchema = false)
abstract class ReportDatabase : RoomDatabase() {
  abstract fun listCategoryDao(): ReportsDao
}