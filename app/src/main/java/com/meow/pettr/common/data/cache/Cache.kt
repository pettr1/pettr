package com.meow.pettr.common.data.cache

import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import io.reactivex.Flowable

interface Cache {
  fun isAnimalInCache(animal: AnimalWithDetails): Boolean
  fun getNearbyAnimals(): Flowable<List<CachedAnimalAggregate>>
  fun getHistoricAnimals(): Flowable<List<CachedAnimalAggregate>>
  fun getFavoriteAnimals(): Flowable<List<CachedAnimalAggregate>>
  fun storeOrganizations(organizations: List<CachedOrganization>)
  fun getOrganization(organizationId: String): CachedOrganization
  fun storeNearbyAnimals(animals: List<CachedAnimalAggregate>)
  fun updateAnimalHistory(history: String, animal: Long)
  suspend fun getAllTypes(): List<String>
  fun removeAnimal(animalId: Long)
  fun clearCache()
  fun clearAllCache()

  fun searchAnimalsBy(): Flowable<List<CachedAnimalAggregate>>
}