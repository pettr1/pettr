package com.meow.pettr.common.data.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reports")
data class Report(@ColumnInfo(name="category_name") var category: String,
                  @ColumnInfo(name="report_info") var reportInfo: String,
                  @ColumnInfo(name="id") @PrimaryKey(autoGenerate = false) var id: String)