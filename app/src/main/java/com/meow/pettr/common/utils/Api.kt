package com.meow.pettr.common.utils

object Api {
    val ERROR_KEY = "error"
    val ROOT_URL = "https://api.petfinder.com/v2/"

    //https://api.petfinder.com/v2/animals?type=dog&breed=Affenpinscher&page=1
    fun buildSearch(): String = ROOT_URL

}