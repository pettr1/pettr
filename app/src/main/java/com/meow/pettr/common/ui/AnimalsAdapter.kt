package com.meow.pettr.common.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.meow.pettr.common.ui.model.UIAnimal
import com.meow.pettr.common.utils.setImage
import com.meow.pettr.databinding.PetHomeCardviewBinding

class AnimalsAdapter: ListAdapter<UIAnimal, AnimalsAdapter.AnimalsViewHolder>(
    ITEM_COMPARATOR
) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalsViewHolder {
    val binding = PetHomeCardviewBinding
        .inflate(LayoutInflater.from(parent.context), parent, false)

    return AnimalsViewHolder(binding)
  }

  override fun onBindViewHolder(holder: AnimalsViewHolder, position: Int) {
    val item: UIAnimal = getItem(position)

    holder.bind(item)
  }

  //connects recyclerView to view holder
  class AnimalsViewHolder(
      private val binding: PetHomeCardviewBinding
  ) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: UIAnimal) {
      binding.petNameData.text = item.name
      binding.petProfilePhoto.setImage(item.photo)
      binding.breedData.text = item.breedPrimary
      binding.genderData.text = item.gender
      binding.profileDetailData.text = item.description
    }
  }
}

private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<UIAnimal>() {
  override fun areItemsTheSame(oldItem: UIAnimal, newItem: UIAnimal): Boolean {
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: UIAnimal, newItem: UIAnimal): Boolean {
    return oldItem.name == newItem.name && oldItem.photo == newItem.photo
  }
}