package com.meow.pettr.common.data.cache

import com.meow.pettr.common.data.cache.model.daos.AnimalsDao
import com.meow.pettr.common.data.cache.model.daos.OrganizationsDao
import com.meow.pettr.common.data.cache.model.cachedanimal.CachedAnimalAggregate
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import io.reactivex.Flowable
import java.time.LocalDateTime
import javax.inject.Inject

class RoomCache @Inject constructor(
    private val animalsDao: AnimalsDao,
    private val organizationsDao: OrganizationsDao
): Cache {

  override fun isAnimalInCache(animal: AnimalWithDetails): Boolean {
    return animalsDao.animalExists(animal.id)
  }

  override fun getNearbyAnimals(): Flowable<List<CachedAnimalAggregate>> {
    return animalsDao.getAllAnimals()
  }

  override fun getHistoricAnimals(): Flowable<List<CachedAnimalAggregate>> {
      return animalsDao.getHistoricAnimals()
  }

  override fun getFavoriteAnimals(): Flowable<List<CachedAnimalAggregate>> {
      return animalsDao.getFavoriteAnimals()
  }

  override fun storeOrganizations(organizations: List<CachedOrganization>) {
    organizationsDao.insert(organizations)
  }

  override fun getOrganization(organizationId: String) : CachedOrganization {
      return organizationsDao.get(organizationId)
  }

  override fun storeNearbyAnimals(animals: List<CachedAnimalAggregate>) {
    animalsDao.insertAnimalsWithDetails(animals)
  }

  override fun updateAnimalHistory(history: String, animalId: Long) {
      val time: String = LocalDateTime.now().toString()
      animalsDao.updateAnimalHistory(history, animalId, time)
  }

  override fun removeAnimal(animalId: Long) {
      animalsDao.delete(animalId)
  }

  override fun clearCache() {
      animalsDao.clearCache()
  }

  override fun clearAllCache() {
      animalsDao.clearAllCache()
      organizationsDao.clearAllCache()
  }

  override suspend fun getAllTypes(): List<String> {
    return animalsDao.getAllTypes()
  }

  override fun searchAnimalsBy(): Flowable<List<CachedAnimalAggregate>> {
    return animalsDao.searchAnimalsBy()
  }
}