package com.meow.pettr.common.data.cache.model.daos

import androidx.room.*
import com.meow.pettr.common.data.cache.model.cachedorganization.CachedOrganization

@Dao
interface OrganizationsDao {

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  fun insert(organizations: List<CachedOrganization>)

  @Transaction
  @Query("SELECT * FROM organizations WHERE organizationId = :organizationId")
  fun get(organizationId: String) : CachedOrganization

  @Transaction
  @Query("DELETE FROM organizations")
  fun clearAllCache()
}