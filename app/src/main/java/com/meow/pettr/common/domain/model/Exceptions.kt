package com.meow.pettr.common.domain.model

class NoMoreAnimalsException(message: String): Exception(message)
class LoadMoreAnimalsException(message: String): Exception(message)
