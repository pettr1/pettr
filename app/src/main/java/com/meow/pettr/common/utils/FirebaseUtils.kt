package com.meow.pettr.common.utils

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

object FirebaseUtils {
    var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    val firebaseDatabase: DatabaseReference = Firebase.database.reference
}

