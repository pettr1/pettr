package com.meow.pettr.common.data.cache.model.cachedTypes

import androidx.room.*
import com.meow.pettr.common.domain.model.type.Breeds

@Entity(tableName = "breeds")
data class CachedBreed(
    @PrimaryKey(autoGenerate = false)
    val typeId: String,
    val breed: String
) {
    companion object {
        fun fromDomain(domainModel: Breeds): CachedBreed {
            return CachedBreed(
                typeId = domainModel.type,
                breed = domainModel.breeds)
        }
    }

    fun toDomain(domainModel: Breeds): Breeds {
        return Breeds(
            typeId,
            breed
        )
    }
}
