package com.meow.pettr.common.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.meow.pettr.common.ui.model.UIAnimal
import com.meow.pettr.common.utils.setImage
import com.meow.pettr.databinding.FragmentPetProfileBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.xml.stream.XMLStreamConstants.CDATA

@AndroidEntryPoint
class PetProfileFragment(currentAnimal: UIAnimal) : Fragment() {

    companion object {

    }

    private val mAnimal = currentAnimal
    private val binding get() = _binding!!
    private var _binding: FragmentPetProfileBinding? = null
    private lateinit var viewModel: PetProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPetProfileBinding.inflate(inflater, container, false);

        _binding!!.petPhoto.setImage(mAnimal.photo)
        _binding!!.tvPetName.text = mAnimal.name
        _binding!!.tvBreed.text = mAnimal.breedPrimary
        _binding!!.tvCoat.text = mAnimal.coat
        _binding!!.tvColor.text = mAnimal.colorPrimary
        _binding!!.tvGender.text = mAnimal.gender
        _binding!!.tvSize.text = mAnimal.size
        _binding!!.tvDescription.text = mAnimal.description
        _binding!!.tvPhone.text = mAnimal.phone
        _binding!!.tvAddress.text = mAnimal.address
        _binding!!.tvCity.text = mAnimal.city
        _binding!!.tvPostcode.text = mAnimal.postcode
        _binding!!.tvCountry.text = mAnimal.country
        _binding!!.tvDistance.text = mAnimal.distance
        _binding!!.tvPhone.text = mAnimal.phone
        _binding!!.tvPhone.setOnClickListener {
            buildPhoneCallIntent()
        }
        _binding!!.tvEmail.text = mAnimal.email
        _binding!!.tvEmail.setOnClickListener {
            buildEmailIntent()
        }

        _binding!!.btnBack.setOnClickListener {
            parentFragmentManager.popBackStack();
        }

        _binding!!.petProfileFragment.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PetProfileViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun buildEmailIntent() {
        val subject = "I'm Interested in adopting ${mAnimal.name}"
        val body = "<p> Hello, <p>" +
                   "<p> I am contacting you to express my intent to adopt ${mAnimal.name}.</p>" +
                   "<p> Thank you. </p>"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("mailto:${mAnimal.email}?subject=$subject&body=${HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY)}")
        startActivity(Intent.createChooser(intent, "Select your Email app"))
    }

    private fun buildPhoneCallIntent() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${mAnimal.phone}")
        startActivity(Intent.createChooser(intent, "Select your Dialer"))
    }

}