package com.meow.pettr.common.data.api.model.mappers

import com.meow.pettr.common.data.api.model.ApiType
import com.meow.pettr.common.domain.model.type.Breeds
import com.meow.pettr.common.domain.model.type.SearchFilter
import javax.inject.Inject

class ApiTypeMapper @Inject constructor():
        ApiMapper<ApiType?, SearchFilter> {

    override fun mapToDomain(apiEntity: ApiType?): SearchFilter {
        return SearchFilter(
            type = apiEntity?.type.orEmpty(),
            coats = apiEntity?.coats.orEmpty(),
            colors = apiEntity?.colors.orEmpty(),
            breeds = mapBreeds(apiEntity)
        )
    }

    private fun mapBreeds(apiTypes: ApiType?): Breeds {
        //call api for breeds
        return Breeds(
            type = apiTypes?.type ?: "",
            breeds = ""
        )
    }
}