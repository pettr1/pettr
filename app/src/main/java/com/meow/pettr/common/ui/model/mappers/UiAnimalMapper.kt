package com.meow.pettr.common.ui.model.mappers

import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.model.organization.Organization
import com.meow.pettr.common.ui.model.UIAnimal
import java.math.BigDecimal
import javax.inject.Inject

class UiAnimalMapper @Inject constructor(): UiMapper<AnimalWithDetails, UIAnimal> {

  override fun mapToView(input: AnimalWithDetails): UIAnimal {
    return UIAnimal(
        id = input.id,
        name = input.name,
        description = input.details.description,
        age = input.details.age.name,
        breedPrimary = input.details.breed.primary,
        breedSecondary = input.details.breed.secondary,
        colorPrimary = input.details.colors.primary,
        colorSecondary = input.details.colors.secondary,
        gender = input.details.gender.name,
        size = input.details.size.sizestring,
        coat = input.details.coat.coatstring,
        history = input.history,
        photo = input.media.getFirstSmallestAvailablePhoto(),
        email = input.details.organization.contact.email,
        phone = input.details.organization.contact.phone,
        address = input.details.organization.contact.address.address1,
        city = input.details.organization.contact.address.city,
        postcode = input.details.organization.contact.address.postcode,
        country = input.details.organization.contact.address.country,
        distance = "%.1f".format(input.details.organization.distance)

    )
  }
}
