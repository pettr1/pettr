package com.meow.pettr.common.di

import com.meow.pettr.common.utils.CoroutineDispatchersProvider
import com.meow.pettr.common.utils.DispatchersProvider
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class ApplicationModule {

  @Binds
  abstract fun bindDispatchersProvider(dispatchersProvider: CoroutineDispatchersProvider):
      DispatchersProvider
}