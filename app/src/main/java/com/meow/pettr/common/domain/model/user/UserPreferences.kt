package com.meow.pettr.common.domain.model.user
import android.content.Context
import android.content.SharedPreferences

class UserPreferences (context: Context){

    val preferences: SharedPreferences = context.getSharedPreferences(PETTR_PREFS, Context.MODE_PRIVATE)

    var animalType: String?
        get() = preferences.getString(ANIMAL_TYPE, "")
        set(value) = preferences.edit().putString(ANIMAL_TYPE, value).apply()
    var animalName: String?
        get() = preferences.getString(ANIMAL_NAME, "")
        set(value) = preferences.edit().putString(ANIMAL_NAME, value).apply()
    var animalBreed: String?
        get() = preferences.getString(ANIMAL_BREED, "")
        set(value) = preferences.edit().putString(ANIMAL_BREED, value).apply()
    var animalAge: String?
        get() = preferences.getString(ANIMAL_AGE, "")
        set(value) = preferences.edit().putString(ANIMAL_AGE, value).apply()
    var animalSize: String?
        get() = preferences.getString(ANIMAL_SIZE, "")
        set(value) = preferences.edit().putString(ANIMAL_SIZE, value).apply()
    var animalGender: String?
        get() = preferences.getString(ANIMAL_GENDER, "")
        set(value) = preferences.edit().putString(ANIMAL_GENDER, value).apply()
    var animalColor: String?
        get() = preferences.getString(ANIMAL_COLOR, "")
        set(value) = preferences.edit().putString(ANIMAL_COLOR, value).apply()
    var animalCoat: String?
        get() = preferences.getString(ANIMAL_COAT, "")
        set(value) = preferences.edit().putString(ANIMAL_COAT, value).apply()
    var animalLocation: String?
        get() = preferences.getString(ANIMAL_LOCATION, "48030")
        set(value) = preferences.edit().putString(ANIMAL_LOCATION, value).apply()
    var animalDistance: Int
        get() = preferences.getInt(ANIMAL_DISTANCE, 100)
        set(value) = preferences.edit().putInt(ANIMAL_DISTANCE, value).apply()
    var userName: String?
        get() = preferences.getString(USER_NAME, "")
        set(value) = preferences.edit().putString(USER_NAME, value).apply()
    var userImage: Int
        get() = preferences.getInt(USER_IMAGE, 1)
        set(value) = preferences.edit().putInt(USER_IMAGE, value).apply()
    var prefsChanged: Boolean
        get() = preferences.getBoolean(PREFS_CHANGE, false)
        set(value) = preferences.edit().putBoolean(PREFS_CHANGE, value).apply()
    var openPetProfileOnFav: Boolean
        get() = preferences.getBoolean(PET_PROFILE_FAV, true)
        set(value) = preferences.edit().putBoolean(PET_PROFILE_FAV, value).apply()
    var isLocationEnabled: Boolean
        get() = preferences.getBoolean(USER_LOCATION_ENABLED, false)
        set(value) = preferences.edit().putBoolean(USER_LOCATION_ENABLED, value).apply()
    var postalCode: String?
        get() = preferences.getString(USER_POSTAL_CODE, "48030")
        set(value) = preferences.edit().putString(USER_POSTAL_CODE, value).apply()

    companion object {
        private const val PETTR_PREFS = "pettr"
        private const val ANIMAL_TYPE = "type"
        private const val ANIMAL_NAME = "name"
        private const val ANIMAL_BREED = "breed"
        private const val ANIMAL_AGE = "age"
        private const val ANIMAL_SIZE = "size"
        private const val ANIMAL_GENDER = "gender"
        private const val ANIMAL_COLOR = "color"
        private const val ANIMAL_COAT = "coat"
        private const val ANIMAL_LOCATION = "location"
        private const val ANIMAL_DISTANCE = "distance"
        private const val USER_NAME = "user_name"
        private const val USER_IMAGE = "user_image"
        private const val PREFS_CHANGE = "prefs_changed"
        private const val PET_PROFILE_FAV = "pet_profile_fav"
        private const val USER_LOCATION_ENABLED = "user_location_enabled"
        private const val USER_POSTAL_CODE = "postal_code"
    }

}
