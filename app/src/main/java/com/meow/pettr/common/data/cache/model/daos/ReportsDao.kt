package com.meow.pettr.common.data.cache.model.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.meow.pettr.common.data.cache.model.Report

@Dao
interface ReportsDao {

  @Query("SELECT * FROM reports")
  fun getAll(): List<Report>

  @Insert
  fun insertAll(vararg report: Report)
}