package com.meow.pettr.user.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseUser
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.ui.AnimalsListAdapter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.PetProfileFragment
import com.meow.pettr.login.LoginActivity
import com.meow.pettr.common.utils.FirebaseUtils.firebaseAuth
import com.meow.pettr.databinding.FragmentHistoryBinding
import dagger.hilt.android.AndroidEntryPoint
import okio.IOException
import retrofit2.HttpException
import javax.inject.Inject

@AndroidEntryPoint
class HistoryFragment @Inject constructor(): Fragment() {

    private val binding get() = _binding!!
    private var _binding: FragmentHistoryBinding? = null
    private lateinit var adapter: AnimalsListAdapter

    private val viewModel: HistoryViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHistoryBinding.inflate(inflater, container, false);

        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        prepareView()
    }

    private fun setupUI() {
        adapter = createAdapter()
        setupRecyclerView(adapter)
        observeViewStateUpdates(adapter)
    }

    private fun createAdapter(): AnimalsListAdapter {
        return AnimalsListAdapter { animal ->
            val petProfile = PetProfileFragment(animal!!)
            parentFragmentManager.beginTransaction().apply {
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                add(com.meow.pettr.R.id.container, petProfile)
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        //Check if user is logged in
        val user: FirebaseUser? = firebaseAuth.currentUser
        if(user == null){
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }
    private fun setupRecyclerView(cacheAdapter: AnimalsListAdapter) {
        binding.histListRecyclerView.apply {
            adapter = cacheAdapter
            layoutManager = GridLayoutManager(requireContext(), 1)
            setHasFixedSize(true)
        }
    }

    private fun prepareView() {
        viewModel.handleEvents(UserEvent.PrepareHistory)
    }

    private fun observeViewStateUpdates(listAdapter: AnimalsListAdapter) {
        viewModel.state.observe(viewLifecycleOwner) {
            updateScreenState(it, listAdapter)
        }
    }

    private fun updateScreenState(newState: UserViewState, listAdapter: AnimalsListAdapter) {
        val (
            inInitialState,
            searchResults,
            noResultsState,
            searchFilter,
            clearAnimalCache,
            failure
        ) = newState

        //updateInitialStateViews(inInitialState)
        listAdapter.submitList(searchResults)
        updateNoResultsViews(noResultsState)
        handleFailures(failure)
    }

    private fun updateNoResultsViews(noResultsState: Boolean) {
        //binding.noSearchResultsImageView.isVisible = noResultsState
        //binding.noSearchResultsText.isVisible = noResultsState
    }

    private fun handleFailures(failure: Event<Throwable>?) {
        val unhandledFailure = failure?.getContentIfNotHandled() ?: return

        handleThrowable(unhandledFailure)
    }

    private fun handleThrowable(exception: Throwable) {
        val fallbackMessage = "An error occurred. Please try again later."
        val snackbarMessage = when (exception) {
            is NoMoreAnimalsException -> exception.message ?: fallbackMessage
            is IOException, is HttpException -> fallbackMessage
            else -> ""
        }

        if (snackbarMessage.isNotEmpty()) {
            Snackbar.make(requireView(), snackbarMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}