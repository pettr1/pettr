package com.meow.pettr.user.usecases

import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.repositories.AnimalRepository
import io.reactivex.Flowable
import javax.inject.Inject

class UpdateAnimalHistory  @Inject constructor(
    private val animalRepository: AnimalRepository
    ) {

        suspend operator fun invoke(history: String, animalId : Long){

            return animalRepository.updateAnimalHistory(history, animalId)

        }
}