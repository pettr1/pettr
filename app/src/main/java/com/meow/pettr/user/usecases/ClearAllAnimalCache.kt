package com.meow.pettr.user.usecases


import com.meow.pettr.common.domain.repositories.AnimalRepository
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.isActive
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class ClearAllAnimalCache @Inject constructor(
    private val animalRepository: AnimalRepository
) {

    suspend operator fun invoke(thisneedstobehere: String) {
        animalRepository.clearAllCache()
        if (!coroutineContext.isActive) {
            throw CancellationException("Cancelled because new data was requested")
        }
    }
}