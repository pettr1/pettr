package com.meow.pettr.user.usecases

import com.meow.pettr.common.domain.model.LoadMoreAnimalsException
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.pagination.Pagination
import com.meow.pettr.common.domain.model.pagination.Pagination.Companion.DEFAULT_PAGE_SIZE
import com.meow.pettr.common.domain.model.type.AvailableSearchFilters
import com.meow.pettr.common.domain.repositories.AnimalRepository
import com.meow.pettr.home.domain.model.HomeParameters
import io.reactivex.Flowable
import io.reactivex.Observable
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.isActive
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class GetTypesRemotely @Inject constructor(
    private val animalRepository: AnimalRepository
) {

  suspend operator fun invoke(): AvailableSearchFilters {
    val types =
        animalRepository.getTypesRemotely()

    if (!coroutineContext.isActive) {
      throw CancellationException("Cancelled because new data was requested")
    }

    return types
  }
}