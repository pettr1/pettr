package com.meow.pettr.user.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.meow.pettr.BuildConfig
import com.meow.pettr.R
import com.meow.pettr.common.utils.FirebaseUtils.firebaseAuth
import com.meow.pettr.common.utils.setImage
import com.meow.pettr.databinding.FragmentAccountBinding
import com.meow.pettr.login.LoginActivity
import com.meow.pettr.userPreferences
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_account.*


@AndroidEntryPoint
class AccountFragment : Fragment() {
    private val binding get() = _binding!!
    private var _binding: FragmentAccountBinding? = null
    private val viewModel: AccountViewModel by viewModels()

    private val permissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            getLastLocation()
        }
        else {
            _binding!!.swLocationServices.isChecked = false
            setPostCode()
        }
    }

    private var mAuthListener = AuthStateListener { fbAuth ->

        if (fbAuth.currentUser != null) {

        } else {
            viewModel.handleEvents(UserEvent.PrepareClearAllCache)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        observeViewStateUpdates()

        _binding = FragmentAccountBinding.inflate(inflater, container, false)

        _binding!!.imgUser.setImage(firebaseAuth.currentUser?.photoUrl.toString())
        _binding!!.tvUserName.text = firebaseAuth.currentUser?.displayName ?: "User"
        _binding!!.tvEmail.text = firebaseAuth.currentUser?.email ?: "email@email.com"
        _binding!!.tvVersion.text = "Version: ${BuildConfig.VERSION_NAME}"

        if (!userPreferences.isLocationEnabled) {
            _binding!!.etPostalCode.setText(userPreferences.postalCode.toString())
            userPreferences.animalLocation = userPreferences.postalCode
        }
        _binding!!.swLocationServices.isChecked = userPreferences.isLocationEnabled

        _binding!!.swProfileSettings.isChecked = userPreferences.openPetProfileOnFav

        _binding!!.swLocationServices.setOnClickListener{
            if(swLocationServices.isChecked) askForLocationPermission()
             else setPostCode()
        }

        _binding!!.swProfileSettings.setOnClickListener{
            userPreferences.openPetProfileOnFav = swProfileSettings.isChecked
        }

        _binding!!.btnLogOut.setOnClickListener{
            firebaseAuth.signOut()
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(requireContext().getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
            val googleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
            googleSignInClient.signOut()
        }

        _binding!!.btnDeleteAccount.setOnClickListener{
            firebaseAuth.currentUser!!.delete()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d("PETTR", "User account deleted.")
                        viewModel.handleEvents(UserEvent.PrepareClearAllCache)
                    } else if(!task.isSuccessful){
                        Log.d("PETTR", "Failed to delete account.")
                    }
                }

        }

        return binding.root;
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(mAuthListener)
    }

    override fun onStop() {
        super.onStop()
        firebaseAuth.removeAuthStateListener(mAuthListener)
    }

    private fun observeViewStateUpdates() {
       viewModel.state.observe(viewLifecycleOwner) {
            updateScreenState(it)
        }
    }

    private fun updateScreenState(newState: UserViewState) {
        val (
            inInitialState,
            searchResults,
            noResultsState,
            searchFilters,
            clearAnimalCache,
            failure
        ) = newState
        if(clearAnimalCache){
            userPreferences.preferences.edit().clear().apply()
            activity?.finish()
            startActivity(Intent(context, LoginActivity::class.java))
        }

    }

    private fun askForLocationPermission(){
        permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        var lastLocation: Location?
        var latText: String = ""
        var longText: String = ""
        var fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
            fusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful && task.result != null) {
                lastLocation = task.result
                latText = (lastLocation)!!.latitude.toString()
                longText = (lastLocation)!!.longitude.toString()
                setLocation("$latText,$longText")
            }
            else {
                Log.w("PETTR", "Get Location:exception", task.exception)
            }

        }
    }

    private fun setLocation(latlong: String){
        if(latlong.isNotEmpty()) {
            userPreferences.animalLocation = latlong
            userPreferences.isLocationEnabled = true
            _binding!!.etPostalCode.setText("")
        }
        else { setPostCode() }
    }

    private fun setPostCode(){
        userPreferences.animalLocation =
            _binding?.etPostalCode?.text.toString()

        userPreferences.isLocationEnabled = false

        _binding!!.etPostalCode.setText(userPreferences.postalCode.toString())
    }



}