package com.meow.pettr.user.ui

sealed class UserEvent {
    object PrepareFavorites : UserEvent()
    object PrepareHistory : UserEvent()
    object PrepareUserPreferences : UserEvent()
    object PrepareSearchFilters : UserEvent()
    object PrepareClearAllCache : UserEvent()
}