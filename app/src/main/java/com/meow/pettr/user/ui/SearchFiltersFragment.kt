package com.meow.pettr.user.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.meow.pettr.AppController.Companion.userPreferences
import com.meow.pettr.R
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.type.SearchFilter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.databinding.FragmentSearchFiltersBinding
import dagger.hilt.android.AndroidEntryPoint
import okio.IOException
import retrofit2.HttpException


@AndroidEntryPoint
class SearchFiltersFragment: Fragment() {

    private val binding get() = _binding!!
    private var _binding: FragmentSearchFiltersBinding? = null
    private val viewModel: SearchFiltersViewModel by viewModels()
    private var _searchFilters: List<SearchFilter> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchFiltersBinding.inflate(inflater, container, false);

        _binding!!.spType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalType = setNull(_binding?.spType?.selectedItem.toString())
                userPreferences?.prefsChanged = true
                setColorandCoat()
            }
        }
        _binding!!.spAge.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalAge = setNull(_binding?.spAge?.selectedItem.toString())
                userPreferences?.prefsChanged = true
            }
        }
        _binding!!.spGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalGender = setNull(_binding?.spGender?.selectedItem.toString())
                userPreferences?.prefsChanged = true
            }
        }
        _binding!!.spColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalColor = setNull(_binding?.spColor?.selectedItem.toString())
                userPreferences?.prefsChanged = true
            }
        }
        _binding!!.spCoat.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalCoat = setNull(_binding?.spCoat?.selectedItem.toString())
                userPreferences?.prefsChanged = true
            }
        }
        _binding!!.spSize.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userPreferences?.animalSize = setNull(_binding?.spSize?.selectedItem.toString())
                userPreferences?.prefsChanged = true
            }
        }

        _binding!!.sbDistance.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, currentValue: Int, p2: Boolean) {
                userPreferences?.animalDistance = currentValue
                _binding!!.tvDistMiles.text = currentValue.toString()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })

        updateSelectedFilters()

        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        prepareView()
    }

    private fun setupUI() {
        observeViewStateUpdates()
    }

    private fun observeViewStateUpdates() {
        viewModel.state.observe(viewLifecycleOwner) {
            updateScreenState(it)
        }
    }

    private fun prepareView() {
        viewModel.handleEvents(UserEvent.PrepareSearchFilters)
    }

    private fun updateScreenState(newState: UserViewState) {
        val (
            inInitialState,
            searchResults,
            noResultsState,
            searchFilters,
            clearAnimalCache,
            failure
        ) = newState

        _searchFilters = searchFilters

        setColorandCoat()
        updateSelectedFilters()
        handleFailures(failure)
    }

    private fun setColorandCoat(){
        if(_searchFilters.isNotEmpty()){
            //set Spinner entries based on Type Selected
            //This will not work. the OnItemSelceted will be called resetting our saved values.
            //Maybe use SavedInstance State or handle additions to spinners in the Adapter created at
            //fragment creation.
            for (filter : SearchFilter in _searchFilters){
                if (filter.type == _binding!!.spType.selectedItem.toString()){

                    var coatsList = arrayListOf<String>().apply {
                        add("Any")
                        for(s in filter.coats) add(s)
                    }
                    var colorList = arrayListOf<String>().apply {
                        add("Any")
                        for(s in filter.colors) add(s)
                    }

                    val coatsAdapter = ArrayAdapter(requireContext(),
                        R.layout.spinner_dropdown_item , coatsList)
                    val colorAdapter = ArrayAdapter(requireContext(),
                        R.layout.spinner_dropdown_item , colorList)

                    _binding!!.spCoat.adapter = coatsAdapter
                    _binding!!.spColor.adapter = colorAdapter

                }
            }
        }
    }

    private fun updateSelectedFilters(){
        userPreferences?.animalName = null
        if(_searchFilters.isNotEmpty()) {
            setColorandCoat()
        }
        selectFilter(_binding!!.spType, userPreferences?.animalType.toString())
        selectFilter(_binding!!.spAge, userPreferences?.animalAge.toString())
        selectFilter(_binding!!.spGender, userPreferences?.animalGender.toString())
        selectFilter(_binding!!.spColor, userPreferences?.animalColor.toString())
        selectFilter(_binding!!.spCoat, userPreferences?.animalCoat.toString())
        selectFilter(_binding!!.spSize, userPreferences?.animalSize.toString())
        _binding!!.sbDistance.progress = userPreferences?.animalDistance ?: 50
        userPreferences?.prefsChanged = true
    }

    private fun selectFilter(spinner: Spinner, value: Any) {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i) == value) {
                spinner.setSelection(i)
                break
            }
        }
    }

    private fun setVal(filter: String?) :String{
        return if(filter == null) "Any" else filter
    }

    private fun setNull(filter: String) :String?{
        return if(filter == "Any") null else filter
    }

    private fun handleFailures(failure: Event<Throwable>?) {
        val unhandledFailure = failure?.getContentIfNotHandled() ?: return

        handleThrowable(unhandledFailure)
    }

    private fun handleThrowable(exception: Throwable) {
        val fallbackMessage = "An error occurred. Please try again later."
        val snackbarMessage = when (exception) {
            is NoMoreAnimalsException -> exception.message ?: fallbackMessage
            is IOException, is HttpException -> fallbackMessage
            else -> ""
        }

        if (snackbarMessage.isNotEmpty()) {
            Snackbar.make(requireView(), snackbarMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}