package com.meow.pettr.user.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.meow.pettr.databinding.FragmentUserProfileBinding
import dagger.hilt.android.AndroidEntryPoint

private const val NUM_PAGES = 2

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private val binding get() = _binding!!
    private var _binding: FragmentUserProfileBinding? = null

    private val viewModel: ProfileViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserProfileBinding.inflate(inflater, container, false);

        val viewPager = binding.pager
        val tabLayout = binding.tabLayout

        val adapter = ProfilePagerAdapter(parentFragmentManager,lifecycle)
        viewPager.adapter = adapter

        TabLayoutMediator(tabLayout,viewPager){tab, position ->
            tab.text = arrayOf("Account","Search")[position]
           viewPager.currentItem = position
        }.attach()

        viewModel.handleEvents(UserEvent.PrepareUserPreferences)

        return binding.root;
    }

    private inner class ProfilePagerAdapter(fa: FragmentManager, lifecycle : Lifecycle) : FragmentStateAdapter(fa,lifecycle) {

        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment {
            when(position){
                0 -> return AccountFragment()
                1 -> return SearchFiltersFragment()
            }
            return SearchFiltersFragment()
        }
    }
}