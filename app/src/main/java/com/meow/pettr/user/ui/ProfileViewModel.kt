package com.meow.pettr.user.ui


import android.util.Log
import androidx.lifecycle.*
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.model.mappers.UiAnimalMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Job
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val uiAnimalMapper: UiAnimalMapper,
    private val compositeDisposable: CompositeDisposable
): ViewModel() {

    val state: LiveData<UserViewState>
        get() = _state

    private val _state: MutableLiveData<UserViewState> = MutableLiveData()
    private var runningJobs = mutableListOf<Job>()

    init {
        _state.value = UserViewState()
    }

    fun handleEvents(event: UserEvent) {
        when(event) {
            is UserEvent.PrepareUserPreferences -> preparePreferences()
        }
    }

    private fun preparePreferences() {
        setupListSubscription()
        setQueryState()
    }

    private fun setupListSubscription() {

    }

    private fun setQueryState() {
        _state.value = state.value!!.copy(noResultsState = false)
    }

    private fun onResults(animals: List<AnimalWithDetails>) {
        if(animals.isEmpty()){
            onEmptyCacheResults()
        } else {
            onAnimalList(animals)
        }
    }

    private fun onEmptyCacheResults() {
        //Show No Animals image In Favorites Fragment
        Log.d("PETTR", "No Favorite Animals")
        _state.value = state.value!!.copy(
            noResultsState = true
        )
    }

    private fun onAnimalList(animals: List<AnimalWithDetails>) {
        _state.value = state.value!!.copy(
            noSearchQueryState = false,
            storedAnimalResults = animals.map { uiAnimalMapper.mapToView(it) },
            noResultsState = false
        )
    }

    private fun onFailure(throwable: Throwable) {
        _state.value = if (throwable is NoMoreAnimalsException) {
            state.value!!.copy(noResultsState = true)
        } else {
            state.value!!.copy(failure = Event(throwable))
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}