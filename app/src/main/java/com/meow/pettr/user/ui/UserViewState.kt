package com.meow.pettr.user.ui

import com.meow.pettr.common.domain.model.type.SearchFilter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.ui.model.UIAnimal

data class UserViewState(
    val noSearchQueryState: Boolean = true,
    val storedAnimalResults: List<UIAnimal> = emptyList(),
    val noResultsState: Boolean = false,
    val searchFilters: List<SearchFilter> = emptyList(),
    val clearAnimalCache: Boolean = false,
    val failure: Event<Throwable>? = null
)
