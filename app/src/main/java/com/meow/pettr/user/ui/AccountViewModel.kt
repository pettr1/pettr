package com.meow.pettr.user.ui


import android.util.Log
import androidx.lifecycle.*
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.type.AvailableSearchFilters
import com.meow.pettr.common.domain.model.type.SearchFilter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.utils.DispatchersProvider
import com.meow.pettr.common.utils.createExceptionHandler
import com.meow.pettr.user.usecases.ClearAllAnimalCache
import com.meow.pettr.user.usecases.GetFavoraiteAnimals
import com.meow.pettr.user.usecases.GetTypesRemotely
import com.meow.pettr.user.usecases.UpdateAnimalHistory
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
    private val dispatchersProvider: DispatchersProvider,
    private val clearAllAnimalCache: ClearAllAnimalCache
): ViewModel() {

    val state: LiveData<UserViewState>
        get() = _state

    private val _state: MutableLiveData<UserViewState> = MutableLiveData()
    private var runningJobs = mutableListOf<Job>()

    init {
        _state.value = UserViewState()
    }

    fun handleEvents(event: UserEvent) {
        when(event) {
            is UserEvent.PrepareClearAllCache -> prepareClearCache()
        }
    }

    private fun prepareClearCache() {
        clearAllAnimalCache()
    }

    private fun clearAllAnimalCache() {
        val exceptionHandler = createExceptionHandler(message = "Failed Clear All Animal Cache.")

        val job = viewModelScope.launch(exceptionHandler) {
            withContext(dispatchersProvider.io()) {
                Log.d("PETTR","Clearing All Animal Cache...")
                clearAllAnimalCache("")
            }

        }
        runningJobs.add(job)
        job.invokeOnCompletion {
            it?.printStackTrace()
            runningJobs.remove(job)
            Log.d("PETTR","DONE Cleared All Animal Cache...")
            onCacheCleared()
        }
    }

    private fun onCacheCleared() {
        _state.value = state.value!!.copy(clearAnimalCache = true)
    }

    private fun createExceptionHandler(message: String): CoroutineExceptionHandler {
        return viewModelScope.createExceptionHandler(message) {
            onFailure(it)
        }
    }

    private fun onFailure(throwable: Throwable) {
       state.value!!.copy(clearAnimalCache = false)
       state.value!!.copy(failure = Event(throwable))
    }

}