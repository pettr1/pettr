package com.meow.pettr.user.usecases

import com.meow.pettr.common.domain.model.animal.AnimalWithDetails
import com.meow.pettr.common.domain.repositories.AnimalRepository
import io.reactivex.Flowable
import javax.inject.Inject

class GetFavoraiteAnimals @Inject constructor(
    private val animalRepository: AnimalRepository
) {

  operator fun invoke(): Flowable<List<AnimalWithDetails>> {

    return animalRepository.getFavoriteAnimals()

  }

}
