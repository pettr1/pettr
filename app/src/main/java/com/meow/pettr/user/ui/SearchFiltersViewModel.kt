package com.meow.pettr.user.ui


import android.util.Log
import androidx.lifecycle.*
import com.meow.pettr.common.domain.model.NoMoreAnimalsException
import com.meow.pettr.common.domain.model.type.SearchFilter
import com.meow.pettr.common.ui.Event
import com.meow.pettr.common.utils.DispatchersProvider
import com.meow.pettr.common.utils.createExceptionHandler
import com.meow.pettr.user.usecases.GetTypesRemotely
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SearchFiltersViewModel @Inject constructor(
    private val dispatchersProvider: DispatchersProvider,
    private val getTypesRemotely: GetTypesRemotely
): ViewModel() {

    val state: LiveData<UserViewState>
        get() = _state

    private val _state: MutableLiveData<UserViewState> = MutableLiveData()
    private var runningJobs = mutableListOf<Job>()

    init {
        _state.value = UserViewState()
    }

    fun handleEvents(event: UserEvent) {
        when(event) {
            is UserEvent.PrepareSearchFilters -> prepareSearchFilters()
        }
    }

    private fun prepareSearchFilters() {
        getSearchFiltersRemotely()
        setQueryState()
    }

    private fun getSearchFiltersRemotely() {
        val exceptionHandler = createExceptionHandler(message = "Failed to search remotely.")

        val job = viewModelScope.launch(exceptionHandler) {
            val filters = withContext(dispatchersProvider.io()) {
                Log.d("PETTR","Searching Types remotely...")
                getTypesRemotely()
            }
            Log.d("PETTR","DONE Searching Types remotely...")
            onResults(filters.types)
        }
        runningJobs.add(job)
        job.invokeOnCompletion {
            it?.printStackTrace()
            runningJobs.remove(job)
        }
    }

    private fun setQueryState() {
        _state.value = state.value!!.copy(noResultsState = false)
    }

    private fun onResults(filters: List<SearchFilter>) {
        if(filters.isEmpty()){
            onEmptyResults()
        } else {
            onFiltersList(filters)
        }
    }

    private fun onEmptyResults() {
        _state.value = state.value!!.copy(
            noResultsState = true
        )
    }

    private fun onFiltersList(filters: List<SearchFilter>) {
        _state.value = state.value!!.copy(
            searchFilters = filters
        )
    }

    private fun createExceptionHandler(message: String): CoroutineExceptionHandler {
        return viewModelScope.createExceptionHandler(message) {
            onFailure(it)
        }
    }

    //if we get a failure set searfilters list to default values.
    private fun onFailure(throwable: Throwable) {
        _state.value = if (throwable is NoMoreAnimalsException) {
            state.value!!.copy(noResultsState = true)
        } else {
            state.value!!.copy(failure = Event(throwable))
        }
    }

}